# Lab 3

## Описание:
Этот проект является форком проекта lab_2. В проект было добавлено использование инструмента ansible-vault для обеспечения безопасности конфиденциальных данных. В файл `config.py` был добавлен метод для распаковки секретов из файла `vault.yml`.

## Проблема с тестами на раннере
Для запуска тестов на раннере приходится использовать ручной процесс дешифрования/шифрования файлов, так как обнаружен баг с библиотекой ansible_vault, который приводит к проблемам с работой на платформе GitLab.

## Требования:
- Python 3.x
- Docker
- Docker Compose

## Использование:
1. Запустите сервисы Docker Compose:
   ```sh
   docker-compose up -d
   ```
3. Открыть `http://localhost:8000/docs`
4. Используйте метод `/predict` для решения задачи
5. Используйте метод `/requests/{requests_id}` для получения  payload данных запроса

## Конфигурация:
- `vault.yml` - файл с кредами для запуска приложения. Перед использованием необходимо добавить код шифровки в файл `vault_password_file'

### Шифрование файла 
```shell
ansible-vault encrypt vault.yml --vault-password-file vault_password_file
```

### Дешифровка файла
```shell
ansible-vault decrypt vault.yml --vault-password-file vault_password_file
```

