import os
from functools import lru_cache
from pathlib import Path

import yaml
from ansible_vault import Vault
from ansible.errors import AnsibleError
from loguru import logger
from pydantic import BaseModel, Field, RedisDsn
from pydantic_settings import BaseSettings, SettingsConfigDict

ROOT_DIR = Path(__file__).parent.parent
DATA_DIR = ROOT_DIR / "data"
LOGS_DIR = ROOT_DIR / "logs"

os.makedirs(LOGS_DIR, exist_ok=True)


def set_secrets_in_environment_variables():
    path = ROOT_DIR / "vault_password_file"
    if not os.path.exists(path):
        raise FileExistsError("File with password for vault not exists")

    vault = Vault(path.read_text())
    secrets_path = ROOT_DIR / 'vault.yml'
    if not os.path.exists(secrets_path):
        raise FileExistsError("File with secrets for vault not exists")

    with open(secrets_path) as secrets:
        file_data = secrets.read()
        try:
            secret_data = vault.load(file_data)
        except AnsibleError as e:
            logger.warning(e)
            secret_data = yaml.safe_load(file_data)

    for key, value in secret_data.items():
        os.environ[str(key)] = str(value)
    logger.info("Secrets set in environment variables")


set_secrets_in_environment_variables()


class KNNModelParams(BaseModel):
    n_neighbors: int = Field(10, title="Кол-во соседей")


class Settings(BaseSettings):
    model_config = SettingsConfigDict(env_file='.env', env_nested_delimiter='__')

    knn_model: KNNModelParams
    redis_dsn: RedisDsn = Field('redis://redis:6379/0', title="Redis DSN")


@lru_cache(maxsize=1)
def get_settings() -> Settings:
    return Settings()
