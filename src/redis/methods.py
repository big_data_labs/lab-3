import json
import uuid

import pandas as pd

from src.redis.engine import redis_client


def set_request_data(request_id: uuid.UUID, data: pd.DataFrame) -> None:
    redis_client.set(request_id, data.to_json())


def get_request_data(request_id: uuid.UUID) -> dict | None:
    request_data = redis_client.get(str(request_id))
    if request_data:
        return json.loads(redis_client.get(str(request_id)))
    return None
