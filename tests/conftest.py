from glob import glob

import pandas as pd
import pytest

from src.api.models import PredictRequest

# redis_service = factories.redis_proc(port=None)


pytest_plugins = [
    fixture.replace("/", ".").replace(".py", "")
    for fixture in glob(
        "**/fixtures.py",
        recursive=True
    )
]


@pytest.fixture
def api_url() -> str:
    return "http://localhost:8000/predict"


@pytest.fixture
def sample_input_data() -> PredictRequest:
    return PredictRequest(
        pregnancies=1,
        glucose=148,
        blood_pressure=72,
        skin_thickness=35,
        insulin=0,
        bmi=33.6,
        diabetes_pedigree_function=0.627,
        age=50
    )


@pytest.fixture
def sample_df_input_data(sample_input_data) -> pd.DataFrame:
    return  pd.DataFrame({
        'Pregnancies': [sample_input_data.pregnancies],
        'Glucose': [sample_input_data.glucose],
        'BloodPressure': [sample_input_data.blood_pressure],
        'SkinThickness': [sample_input_data.skin_thickness],
        'Insulin': [sample_input_data.insulin],
        'BMI': [sample_input_data.bmi],
        'DiabetesPedigreeFunction': [sample_input_data.diabetes_pedigree_function],
        'Age': [sample_input_data.age]
    })
